/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller do extrato da aplicação
 *
 ************************/

angular.module('starter.extratoController', [])

.controller('extratoController', function($scope, $ionicModal, $timeout, checkCPF, $state, $ionicPopup, getPontos, listExtract) {

	$scope.getData = function (){
		getPontos(
				window.localStorage.userCpf,
				function (response){
					if (response.data.status.erro) {
						$ionicPopup.alert({
                       		title: 'Ops',
                        	template: 'Parece que aconteceu algo errado, verifique e tente novamente'
	                	});
                    } else {
                        // ccaso aja dados cadastrados
                        $scope.saldo  = response.data.data;
                    }
				},
				function (response){
				 	$ionicPopup.alert({
                        title: 'Ops',
                        template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
	                });
				}
			);
		
		listExtract(
			window.localStorage.userCpf,
			function (response){
				if (response.data.status.erro) {
						//caso não aja dados cadastrados
						$ionicPopup.alert({
                            title: 'Ops',
                            template: 'Parece tem algo errado, verifique e tente novamente'
                		});
                    } else {
                    	console.log(response.data);
                        $scope.extractList = response.data.data;
                    }
			},
			function (response){
				$ionicPopup.alert({
	            title: 'Ops',
	            template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
	        	});
			}
			);
	}


});
