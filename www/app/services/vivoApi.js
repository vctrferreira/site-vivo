/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 11/11/2015
 *  Função: Factory da aplicação
 *
 ************************/

angular.module('starter.vivoService', [])
	.constant('vivo', {
		"promotores":"http://campanhaturbinesuasvendas.com.br/api/DAOPromotores.cfc",
    "planos":"http://campanhaturbinesuasvendas.com.br/api/DAOPlanos.cfc",
    "ativacoes":"http://campanhaturbinesuasvendas.com.br/api/DAOAtivacoes.cfc"
	})


	.factory('requisicao', function ($http, vivo) {

    return function (url, success, error, method, params) {

        	$http({
        	   'url': url+'?method='+method+params
       		})
          .then(
       		 function successCallback(response) {
	    		   if (typeof (success) == "function")
               	success(response);
  			   },
  			   function errorCallback(response) {
  			    if (typeof (error) == "function")
                	error(response);
 			      }
          );
		  }
	 })

/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 12/11/2015
 *  Função: factory para requisição do login
 *
 ************************/

	.factory('login', function (requisicao, vivo) {

    return function (loginData, success, error, remember) {

        requisicao(
          vivo.promotores,
          function (response) {
           if (typeof (success) == "function"){
              if(remember){
                  window.localStorage['auth'] = JSON.stringify(loginData);
                }
              success(response);
           }
          },
          function (response) {
              if (typeof (error) == "function")
                  error(response);
          },
          'login',
          ('&CPF='+loginData.cpf+'&senha='+loginData.senha)
        );
    };
  })

  .factory('checkCPF', function (requisicao, vivo){
    return function (cpf, success, error){
      requisicao(
        vivo.promotores,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'checkCPF',
        ('&CPF='+cpf)
        );
    }
  })
  .factory('sendPassword', function (requisicao, vivo){
    return function (cpf, success, error){
      requisicao(
        vivo.promotores,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'sendPassword',
        ('&CPF='+cpf)
        );
    }
  })

 .factory('listPlanos', function (requisicao, vivo){
    return function (success, error){
      requisicao(
        vivo.planos,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'listBasic',
        ''
        );
    }
  })
 .factory('sendVenda', function (requisicao, vivo){
    return function (venda, success, error){


      var str = [];
      for (var p in venda)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(venda[p]));    
      str = str.join("&");
      requisicao(
        vivo.ativacoes,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'insert',
        '&'+str
        );
    }
  })

 .factory('listVendas', function (requisicao, vivo){
    return function (cpf, success, error){
      requisicao(
        vivo.ativacoes,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'list',
        ('&CPF='+cpf)
        );
    }
  })
 .factory('updatePassword', function (requisicao, vivo){
    return function (dados, success, error){
      requisicao(
        vivo.promotores,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'update',
        ('&CPF='+dados.CPF+'&senha='+dados.senha)
        );
    }
  })
.factory('getPontos', function (requisicao, vivo){
    return function (cpf, success, error){
      requisicao(
        vivo.promotores,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'points',
        ('&CPF='+cpf)
        );
    }
  })
.factory('listExtract', function (requisicao, vivo){
    return function (cpf, success, error){
      requisicao(
        vivo.ativacoes,
        function (response){
          if(typeof (success) == "function")
            success(response);
        },
        function (response){
          if(typeof (success) == "function")
            error(response);
        },
        'extract',
        ('&CPF='+cpf)
        );
    }
  })
.factory('email', function (requisicao, vivo) {

    return function (ajudaData, success, error) {

        requisicao(
          vivo.promotores,
          function (response) {
           if (typeof (success) == "function"){
              success(response);
           }
          },
          function (response) {
              if (typeof (error) == "function")
                  error(response);
          },
          'sendEmailToGC',
          ('&CPF='+window.localStorage.userCpf+'&mensagem='+ajudaData.mensagem)
        );
    };
  });



 
