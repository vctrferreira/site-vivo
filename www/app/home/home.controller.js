/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.homeController', [])

.controller('homeController', function ($scope, $state, $rootScope, $cordovaSQLite, $ionicLoading, $ionicPopup, sendVenda) {

    $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams){ 
        // Mostra a logo na navbar apenas se for a navbar da home
        $rootScope.showBrandLogo = toState.name == "app.home";
    
    })
    $scope.userName = window.localStorage.userName;

    // Mostra a logo na navbar apenas se for a navbar da home
    $rootScope.removeNavbarBackground = $state.current.name == "app.home";
    console.log($rootScope.removeNavbarBackground);


    $scope.logout = function (){
    	window.localStorage.clear();
    	$state.go('app.login');
    }

});
