/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.ajudaController', ['starter.vivoService'])

.controller('ajudaController', function($scope, email, $state, $rootScope, $ionicPopup) {


	$scope.ajudaData = {};
	$scope.nomeCG = window.localStorage.nomeCG; //resgato o valor para exibir no formulario
	$scope.celularGC = window.localStorage.celularGC; //resgato o valor para exibir no formulario

    // Mostra a logo na navbar apenas se for a navbar da home
    $rootScope.showBrandLogo = $state.current.name == "app.home";
   	$scope.doAjuda = function() {

        email($scope.ajudaData,
                function(response) {

                    if (response.data.status.erro == false) { //se o retorno for sucesso

                        alert('Mensagem enviada com sucesso');
                    } else { //caso o servidor retorne algum erro
                     
                         alert('Erro ao enviar a mensagem');
                    }
                });



    }


});
