/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.dadosController', [])

.controller('dadosController', function($scope, $ionicModal, $timeout, checkCPF, $state, $ionicPopup, validaCPF, updatePassword) {

	$scope.dados = {};
	$scope.cadastro = {};
	



	$scope.getDados = function (){
	
			$scope.regulamento = {value:true};

			$scope.termos = false;
			$scope.btnMSG = "ALTERAR SENHA";

			$scope.msgUpdate = {};
			$scope.msgUpdate.titulo = "Sucesso.";
			$scope.msgUpdate.texto = "Senha cadastrada.";
			console.log('gere');
		
		$scope.dados = JSON.parse(window.localStorage.userLogado);
		console.log($scope.dados);
		window.localStorage.userLogado = null;
	}


	$scope.setDataUpdateSenha = function (){
		window.localStorage.userLogado = JSON.stringify($scope.dados);
		$state.go('app.update_SenhaLogado');
	}

	$scope.sendUpdateSenha = function (){

		

		if ($scope.dados.senha != $scope.dados.repSenha) {
			$ionicPopup.alert({
                    title: 'Ops!',
                    template: 'As senhas não conferem.'
                });
			return;
		}

		updatePassword(
			$scope.dados,
			function (response){
				if (response.data.status.erro) {
						//caso não aja dados cadastrados
						$ionicPopup.alert({
                            title: 'Ops',
                            template: 'Parece tem algo errado, verifique e tente novamente'
                		});
                    } else {
                        $ionicPopup.alert({
                            title: $scope.msgUpdate.titulo,
                            template: $scope.msgUpdate.texto,
                            okText: 'Fazer login'
                		});
                        
                        if(window.localStorage.userCpf){
                        	window.localStorage.clear();
                        }
                        $state.go('app.login');

                    }
			},
			function (response){
				$ionicPopup.alert({
	            	title: 'Ops',
	           	 	template: 'Parece que ocorreu algo errado na comunicação, tente de novo mais tarde.'
	        	});

			}
		);
	}

});
