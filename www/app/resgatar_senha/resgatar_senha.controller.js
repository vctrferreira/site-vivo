/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.resgatarSenhaController', [])

.controller('resgatarSenhaController', function($scope, $state, $rootScope, $ionicPopup, validaCPF, sendPassword) {


	$scope.resgate = {};
    // Mostra a logo na navbar apenas se for a navbar da home
    $rootScope.showBrandLogo = $state.current.name == "app.home";
    $scope.verificaCPF = function (){
		if(validaCPF($scope.resgate.cpf)){
			sendPassword(
				$scope.resgate.cpf,
				function (response){
					if (response.data.status.erro) {
							//caso não aja dados cadastrados
							$ionicPopup.alert({
                            	title: 'Ops!',
                            	template: 'Este CPF ainda não está cadastrado na campanha.',
                            	okText:'CADASTRE-SE'
                			});

                			$state.go('app.cadastrar');
	                    } else {
	                        // ccaso aja dados cadastrados
	                        $ionicPopup.alert({
                            	title: 'Sucesso!',
                            	template: 'Sua senha foi enviada por SMS para o celular cadastrado.',
                            	okText: 'VOLTAR AO LOGIN'
                			});
                			$state.go('app.login');
	                    }
				},
				function (response){
				 	$ionicPopup.alert({
	                            title: 'Ops',
	                            template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
	                });
				}
			);
		}else{
            $ionicPopup.alert({ //caso o cpf seja invalido
                title: 'CPF Inválido',
                template: 'Exite algo errado no seu CPF, verifique e tente novamente.'
            });
        }
	}
	

});
