angular.module('starter', ['ionic',
                           'ngMask',
                           'ngCordova',
                           'starter.homeController',
                           'starter.mainController',
                           'starter.loginController',
                           'starter.menuController',
                           'starter.cadastrarController',
                           'starter.resgatarSenhaController',
                           'starter.vendasController',
                           'starter.extratoController',
                           'starter.ajudaController',
                           'starter.dadosController',
                           'ui.bootstrap'])

.run(function ($ionicPlatform, $cordovaSQLite, $rootScope, $state) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    
    });


    
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Ajeita o backbutton para ficar no estilo da vivo
    $ionicConfigProvider.backButton.icon('ion-arrow-left-b');
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.backButton.text('');

    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/menu/menu.html',
        controller: 'menuController'
    })

    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'app/login/login.html',
                controller: 'loginController'
            }
        }
    })

    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'app/home/home.html',
                controller: 'homeController'
            }
        }
    })

    .state('app.cadastrar', {
        url: '/cadastrar',
        views: {
            'menuContent': {
                templateUrl: 'app/cadastrar/checarCpf.html',
                controller: 'cadastrarController'
            }
        }
    })

    .state('app.cadastrar_exibirCpf', {
        url: '/cadastrar/exibirCpf',
        views: {
            'menuContent': {
                templateUrl: 'app/cadastrar/exibirCpf.html',
                controller: 'cadastrarController'
            }
        }
    })

    .state('app.extrato', {
        url: '/extrato',
        views: {
            'menuContent': {
                templateUrl: 'app/extrato/extrato.html',
                controller: 'extratoController'
            }
        }
    })


    .state('app.cadastrar_novaVenda', {
        url: '/novaVenda',
        views: {
            'menuContent': {
                templateUrl: 'app/vendas/novaVenda.html',
                controller: 'vendasController'
            }
        }
    })

    .state('app.resgatar_senha', {
            url: '/resgatar_senha',
            views: {
                'menuContent': {
                    templateUrl: 'app/resgatar_senha/checarCpf.html',
                    controller: 'resgatarSenhaController'
                }
            }
        })
        .state('app.listar_Vendas', {
            url: '/minhas_vendas',
            views: {
                'menuContent': {
                    templateUrl: 'app/vendas/listVenda.html',
                    controller: 'vendasController'
                }
            }
        })
        .state('app.update_SenhaCadastrado', {
            url: '/update_senha_cadastrado',
            views: {
                'menuContent': {
                    templateUrl: 'app/cadastrar/novaSenha.html',
                    controller: 'cadastrarController'
                }
            }
        })
        .state('app.ajuda', {
            url: '/ajuda',
            views: {
                'menuContent': {
                    templateUrl: 'app/ajuda/ajuda.html',
                    controller: 'ajudaController'
                }
            }
        })
        .state('app.regulamento', {
            url: '/regulamento',
            views: {
                'menuContent': {
                    templateUrl: 'app/regulamento/regulamento.html',
                    controller: 'homeController'
                }
            }
        })
        .state('app.dados', {
            url: '/dados',
            views: {
                'menuContent': {
                    templateUrl: 'app/dados/dados.html',
                    controller: 'dadosController'
                }
            }
        })
        .state('app.extrato_Pontos', {
            url: '/extrato_pontos',
            views: {
                'menuContent': {
                    templateUrl: 'app/extrato/extrato.html',
                    controller: 'extratoController'
                }
            }
        })
        .state('app.venda_NaoCadastrado', {
            url: '/venda_nao_cadastro',
            views: {
                'menuContent': {
                    templateUrl: 'app/vendas/novaVendaNaoCadastrado.html',
                    controller: 'vendasController'
                }
            }
        });

    $urlRouterProvider.otherwise('app/login');
});
