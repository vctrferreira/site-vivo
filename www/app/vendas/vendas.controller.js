	/************************
	 *
	 *  Autor: Swellit Solutions
	 *  Data: 13/11/2015
	 *  Função: Controller das vendas da aplicação
	 *
	 ************************/

	 angular.module('starter.vendasController', [])

	 .controller('vendasController', function($scope, $state, $rootScope, $ionicPopup, $ionicLoading, $cordovaSQLite,listPlanos, listVendas, sendVenda) {

	 	$rootScope.showBrandLogo = $state.current.name == "app.home";

	 	$scope.venda = {};
	 	$scope.planos = [];
	 	$scope.getDados = function (){
	 		

	 		listPlanos(
	 			function (response){
	 				$scope.planos = response.data.data;
					console.log($scope.planos);
				},
				function (response){
					console.log(response);
				}
				);
	 	}

	 	$scope.enviaNovaVenda = function (){
	 		var date = $scope.venda.dataRegistro.split('/');
	 		var dateTime = new Date(date[2], date[1], date[0]);


	 		if (dateTime < new Date()) {
	 			$ionicPopup.alert({
							title: 'Ops!',
							template: 'Data inválida.'
						});
	 			return;
	 		}
	 		if($scope.venda.linha.length < 15){
				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Linha inválida.'
					});
	 			return;	 		
	 		}
	 		if($scope.venda.idPlano == undefined){
 				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Selecione um plano.'
					});
	 			return;
	 		}

	 		$scope.venda.CPF = window.localStorage.userCpf;

	 		sendVenda($scope.venda, function (response){

 				if (response.data.status.erro) {
						//caso não aja dados cadastrados
						$ionicPopup.alert({
							title: 'Ops',
							template: 'Parece tem algo errado, verifique e tente novamente'
						});
				} else {
					$ionicPopup.alert({
						title: 'Sucesso',
						template: response.data.status.mensagem
					});
					$state.go('app.home');
				}
			},

			function (response){
				var query = "INSERT INTO vendas (cpf, idPlano, dataRegistro, linha) VALUES (?,?,?,?)";
				$cordovaSQLite.execute(db, query, [$scope.venda.CPF, $scope.venda.idPlano, $scope.venda.dataRegistro, $scope.venda.linha])
				.then(function(res) {
					console.log("INSERT ID -> " + res.insertId);
				}, function (err) {
					console.error(err);
				});
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que você está sem internet, armazenamos sua venda, sincronize assim que possível'
				});

				$state.go('app.home');
			});
		}


		$scope.listarVendas = function(){

			$ionicLoading.show({
	            template: 'Baixando lista de vendas...'
	        });
			listVendas(window.localStorage.userCpf, function (response){
				if (response.data.status.erro) {
					//caso não aja dados cadastrados
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Ops',
						template: 'Parece tem algo errado, verifique e tente novamente'
					});
					$state.go('app.home');
				} else {
					console.log(response.data);
					$scope.vendasList = response.data.data;
					$ionicLoading.hide();
				}
			},
			function (response){
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
				});
				$state.go('app.home');
			});
		}




		$scope.vendaNaoCadastrada = function (){

			$state.go('app.venda_NaoCadastrado');
		}



	 	$scope.enviaNovaVendaNaoCadastrado = function (){
	 		var date = $scope.venda.dataRegistro.split('/');
	 		var dateTime = new Date(date[2], date[1], date[0]);


	 		if (dateTime < new Date()) {
	 			$ionicPopup.alert({
							title: 'Ops!',
							template: 'Data inválida.'
						});
	 			return;
	 		}
	 		if($scope.venda.linha.length < 15){
				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Linha inválida.'
					});
	 			return;	 		
	 		}
	 		if($scope.venda.idPlano == undefined){
 				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Selecione um plano.'
					});
	 			return;
	 		}


	 		sendVenda($scope.venda, function (response){

 				if (response.data.status.erro) {
						//caso não aja dados cadastrados
						$ionicPopup.alert({
							title: 'Ops',
							template: 'Parece tem algo errado, verifique e tente novamente'
						});
				} else {
					$ionicPopup.alert({
						title: 'Sucesso',
						template: response.data.status.mensagem
					});
					$state.go('app.login');
				}
			},

			function (response){
				var query = "INSERT INTO vendas (cpf, idPlano, dataRegistro, linha) VALUES (?,?,?,?)";
				$cordovaSQLite.execute(db, query, [$scope.venda.CPF, $scope.venda.idPlano, $scope.venda.dataRegistro, $scope.venda.linha])
				.then(function(res) {
					console.log("INSERT ID -> " + res.insertId);
				}, function (err) {
					console.error(err);
				});
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que você está sem internet, armazenamos sua venda, sincronize assim que possível'
				});

				$state.go('app.login');
			});
		}

});
