/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller do menu da aplicação
 *
 ************************/

angular.module('starter.menuController', [])

.controller('menuController', function($scope, $state) {


	$scope.getDetails = function(){

		$state.go('app.logado_exibirCpf');
	}
	$scope.minhasVendas = function (){
		$state.go('app.listar_Vendas');
	}
	$scope.novaVenda = function (){
		$state.go('app.cadastrar_novaVenda');
	}

	$scope.extratorPontos = function (){
		$state.go('app.extrato_Pontos');
	}

	
	$scope.hotSite = function (){
		window.open('http://campanhaturbinesuasvendas.com.br', '_blank', 'location=yes');		
	}

	$scope.regulamento = function (){
		window.open('http://campanhaturbinesuasvendas.com.br/regulamento.pdf', '_blank', 'location=yes');
	}

	$scope.ajuda = function (){
		$state.go('');
	}




	$scope.logout = function (){
		window.localStorage.clear();
		$state.go('app.login');
	}


});
